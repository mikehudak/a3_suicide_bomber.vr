/*
	AUTHOR: MikeSK 701st [S.O.G.]
	NAME: suicide_bomber.sqf
	VERSION: 1.0
	
	DESCRIPTION:
	Make suicide bomber from unit. Bomber will follow any unit in area 100m. When bomber close to targer (15m) he will shout and detonate. Unit can by any side. Script will add bandana and suicide vest.
	
	USAGE:
	Add this line to unit init:
	null = [unitName, targetSide] execVM "scripts\suicide_bomber.sqf";
	Example:
	null = [this, WEST] execVM "scripts\suicide_bomber.sqf";

	CONFIG:
	unitName: STRING - name of unit that become suicide bomber (default this)
	targetSide: ARRAY - define array of sides that suicide bomber will attack (default all sides)
*/
if (!isServer) exitWith {};

private ["_bomber", "_targetSide", "_explosives", "_explosiveClass", "_stopLoop", "_nearUnits", "_position", "_explosive"];
params [
	"_bomber", 		//unit
	"_targetSide"	//side
];

_targetSide = [_this, 1, [civilian, west, east, independent]] call BIS_fnc_param;
_bomber setVariable ["suicideBomber", true, false];

removeAllWeapons _bomber;
removeAllItems _bomber;
removeAllAssignedItems _bomber;
removeVest _bomber;
removeBackpack _bomber;
removeHeadgear _bomber;
removeGoggles _bomber;

_bomber addVest "V_HarnessOGL_gry";
_bomber addGoggles "G_Bandanna_tan";

_explosives = ["M_Titan_AP", "HelicopterExploBig", "Bo_GBU12_LGB_MI10", "HelicopterExploSmall", "R_80mm_HE", "R_TBG32V_F"];
_explosiveClass = _explosives call BIS_fnc_selectRandom;

_stopLoop = 0;

while {alive _bomber && _stopLoop == 0} do
{
	waitUntil {time > 1};

	_nearUnits = nearestObjects [_bomber, ["CAManBase"], 100];
	_nearUnits = _nearUnits - [_bomber];
	{
		if(!(side _x in _targetSide) or (_x getVariable ["suicideBomber", false])) then 
		{
			_nearUnits = _nearUnits - [_x];
		};
	} forEach _nearUnits;
	
	if(count _nearUnits != 0) then
	{
		_position = position (_nearUnits select 0);
		_bomber doMove _position;
		_bomber setUnitPos "UP";
		_bomber allowFleeing 0;
		
		if(_bomber distance (_nearUnits select 0) < 15) exitWith 
		{
			_stopLoop = 1;
			_bomber say3D "bomber";
			_bomber addRating -10000000;
			sleep 2;
			_explosive = _explosiveClass createVehicle (getPos _bomber);
			_explosive setDamage 1; 
			waitUntil {!alive _bomber};
			deleteVehicle _explosive;
		};
	};
	
	sleep 1;
};